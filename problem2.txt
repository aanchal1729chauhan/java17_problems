Q.2 Given an integer numRows, return the first numRows of Pascal's triangle.

In Pascal's triangle, each number is the sum of the two numbers directly above it.
code:

class Solution {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>>res = new ArrayList<List<Integer>>();
        List<Integer>temp = new ArrayList<Integer>();
        temp.add(1);
        res.add(temp);
        for(int i=1;i<numRows;i++){
            List<Integer>curr = new ArrayList<Integer>();
            curr.add(1);
            for(int j=0;j<temp.size()-1;j++){
                curr.add(temp.get(j)+temp.get(j+1));
            }
            curr.add(1);
            res.add(curr);
            temp = curr;
        }
        return res;
        

        
        
    }
}